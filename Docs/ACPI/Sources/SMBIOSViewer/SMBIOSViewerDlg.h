// SMBIOSViewerDlg.h : header file
//

#if !defined(AFX_SMBIOSVIEWERDLG_H__A209EDEF_CBEF_4226_B60B_34B110DFAAE7__INCLUDED_)
#define AFX_SMBIOSVIEWERDLG_H__A209EDEF_CBEF_4226_B60B_34B110DFAAE7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "XGListCtrl.h"
#include "SMBiosStructs.h"

/////////////////////////////////////////////////////////////////////////////
// CSMBIOSViewerDlg dialog

class CSMBIOSViewerDlg : public CDialog
{
// Construction
public:
	CSMBIOSViewerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSMBIOSViewerDlg)
	enum { IDD = IDD_SMBIOSVIEWER_DIALOG };
	CComboBox	m_oTableType;
	XGListCtrl	m_oRawDataList;
	CString	m_szTableInfo;
	//}}AFX_DATA
	void AddTable(EnumTableStruct* pstEnumTableStruct);

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSMBIOSViewerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL
	
// Implementation
protected:
	HICON m_hIcon;
	SMBiosData	m_oSMBIOSData;

	void PopulateHeaderDetails(SMBios_TypeBase* pstSMBios_TypeBase,CString& szText);
	// Generated message map functions
	//{{AFX_MSG(CSMBIOSViewerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSelchangeComboTableType();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMBIOSVIEWERDLG_H__A209EDEF_CBEF_4226_B60B_34B110DFAAE7__INCLUDED_)
