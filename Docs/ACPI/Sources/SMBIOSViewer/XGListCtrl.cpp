// XGListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "XGListCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// XGListCtrl
#define NUM_CHARS_PER_LINE						05

XGListCtrl::XGListCtrl()
{
	m_oFont.CreatePointFont(8,_T("FixedSys"),NULL);
	m_oBrush.CreateStockObject(HOLLOW_BRUSH);

	m_oHighlightBrush.CreateSolidBrush(RGB(0,255,255));

	CDC oMemDC;
	CDC* pDC = GetDesktopWindow()->GetDC();
	
	oMemDC.CreateCompatibleDC(pDC);

	CFont* pOldFont = oMemDC.SelectObject(&m_oFont);
	CSize oSize = oMemDC.GetTextExtent(_T(" "));
	m_nCharWidth = oSize.cx*3;
	oMemDC.SelectObject(pOldFont);
	m_nStart = 0; 
	m_nEnd = 0;

	m_nNumCharsPerLine = NUM_CHARS_PER_LINE;
}

XGListCtrl::~XGListCtrl()
{
}


BEGIN_MESSAGE_MAP(XGListCtrl, CListCtrl)
	//{{AFX_MSG_MAP(XGListCtrl)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// XGListCtrl message handlers
void XGListCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct )
{
	CString szItemText;
	szItemText = GetItemText(lpDrawItemStruct->itemID,0);
	CDC dc;

	dc.Attach(lpDrawItemStruct->hDC);

	RECT rcItem = lpDrawItemStruct->rcItem;
	
	//now draw borders
	//is line supposed to be bordered
	int nStartLine = m_nStart/m_nNumCharsPerLine;
	int nEndLine = m_nEnd/m_nNumCharsPerLine;

	if(lpDrawItemStruct->itemID >= nStartLine && 
	   lpDrawItemStruct->itemID <= nEndLine	)
	{
		if(nStartLine == nEndLine)
		{
			//just one rectangle
			lpDrawItemStruct->rcItem.left += (m_nStart%m_nNumCharsPerLine)*m_nCharWidth;
			lpDrawItemStruct->rcItem.right = lpDrawItemStruct->rcItem.left + ((m_nEnd - m_nStart)%m_nNumCharsPerLine)*m_nCharWidth;
			//dc.Rectangle(&lpDrawItemStruct->rcItem);
			dc.FillRect(&lpDrawItemStruct->rcItem,&m_oHighlightBrush);
			dc.MoveTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top);
			dc.LineTo(lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.top);
			dc.LineTo(lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.bottom);
			dc.LineTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.bottom);
			dc.LineTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top);
		}
		else
		{
			if(lpDrawItemStruct->itemID != nStartLine &&
			   lpDrawItemStruct->itemID != nEndLine	)
			{
				//simply draw vertical lines at the end
				RECT rc = lpDrawItemStruct->rcItem;
				rc.right = lpDrawItemStruct->rcItem.left + (m_nNumCharsPerLine)*m_nCharWidth;
				dc.FillRect(&rc,&m_oHighlightBrush);
				dc.MoveTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top);
				dc.LineTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.bottom);
				int nRight = lpDrawItemStruct->rcItem.left + (m_nNumCharsPerLine)*m_nCharWidth;
				dc.MoveTo(nRight,lpDrawItemStruct->rcItem.top);
				dc.LineTo(nRight,lpDrawItemStruct->rcItem.bottom);
			}
			else if(lpDrawItemStruct->itemID == nStartLine)
			{
				if((nEndLine == nStartLine + 1) && 
					m_nStart%m_nNumCharsPerLine > m_nEnd%m_nNumCharsPerLine)
				{
					//just one rectangle
					lpDrawItemStruct->rcItem.right = lpDrawItemStruct->rcItem.left + (m_nNumCharsPerLine)*m_nCharWidth;
					lpDrawItemStruct->rcItem.left = (m_nStart%m_nNumCharsPerLine)*m_nCharWidth;
					//dc.Rectangle(&lpDrawItemStruct->rcItem);
					dc.FillRect(&lpDrawItemStruct->rcItem,&m_oHighlightBrush);
					dc.MoveTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top);
					dc.LineTo(lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.top);
					dc.LineTo(lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.bottom);
					dc.LineTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.bottom);
					dc.LineTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top);
				}
				else
				{
					int nLeft = lpDrawItemStruct->rcItem.left + ((m_nStart)%m_nNumCharsPerLine)*m_nCharWidth;
					RECT rc = lpDrawItemStruct->rcItem;
					rc.left = nLeft;
					rc.right = lpDrawItemStruct->rcItem.left + (m_nNumCharsPerLine)*m_nCharWidth;
					dc.FillRect(&rc,&m_oHighlightBrush);
					dc.MoveTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.bottom);
					dc.LineTo(nLeft,lpDrawItemStruct->rcItem.bottom);
					dc.LineTo(nLeft,lpDrawItemStruct->rcItem.top);
					int nRight = lpDrawItemStruct->rcItem.left + (m_nNumCharsPerLine)*m_nCharWidth;
					dc.LineTo(nRight,lpDrawItemStruct->rcItem.top);
					dc.LineTo(nRight,lpDrawItemStruct->rcItem.bottom);
				}

			}
			else if(lpDrawItemStruct->itemID == nEndLine)
			{
				if((nEndLine == nStartLine + 1) && 
					m_nStart%m_nNumCharsPerLine > m_nEnd%m_nNumCharsPerLine)
				{
					//just one rectangle
					lpDrawItemStruct->rcItem.right = lpDrawItemStruct->rcItem.left + ((m_nEnd)%m_nNumCharsPerLine)*m_nCharWidth;
					//dc.Rectangle(&lpDrawItemStruct->rcItem);
					dc.FillRect(&lpDrawItemStruct->rcItem,&m_oHighlightBrush);
					dc.MoveTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top);
					dc.LineTo(lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.top);
					dc.LineTo(lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.bottom);
					dc.LineTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.bottom);
					dc.LineTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top);
				}
				else if(m_nEnd%m_nNumCharsPerLine)
				{
					RECT rc = lpDrawItemStruct->rcItem;
					rc.right = lpDrawItemStruct->rcItem.left + ((m_nEnd)%m_nNumCharsPerLine)*m_nCharWidth;
					dc.FillRect(&rc,&m_oHighlightBrush);

					dc.MoveTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top);
					dc.LineTo(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.bottom);
					int nRight = lpDrawItemStruct->rcItem.left + ((m_nEnd)%m_nNumCharsPerLine)*m_nCharWidth;
					dc.LineTo(nRight,lpDrawItemStruct->rcItem.bottom);
					dc.LineTo(nRight,lpDrawItemStruct->rcItem.top);
					dc.LineTo(lpDrawItemStruct->rcItem.left + (m_nNumCharsPerLine)*m_nCharWidth,lpDrawItemStruct->rcItem.top);
				}
			}
		}
		
	}

	CFont* pOldFont = (CFont*)dc.SelectObject(&m_oFont);
	CBrush* pOldBrush = (CBrush*)dc.SelectObject(&m_oBrush);

	int nOldBkMode = dc.SetBkMode(TRANSPARENT);

	dc.DrawText(szItemText,&rcItem,DT_LEFT|DT_SINGLELINE);

	dc.SetBkMode(nOldBkMode);

	dc.SelectObject(pOldFont);

	dc.SelectObject(pOldBrush);

	dc.Detach();
}

BOOL XGListCtrl::SetStartEnd(int nStart,int nEnd)
{
	BOOL bRet = FALSE;
	if(nEnd >= nStart)
	{
		m_nStart = nStart;
		m_nEnd = nEnd;

		Invalidate();
		UpdateWindow();
		bRet = TRUE;
	}
	return bRet;
}

void XGListCtrl::SetNumCharsPerLine(int nNumCharsPerLine)
{
	m_nNumCharsPerLine = nNumCharsPerLine;
	if(GetSafeHwnd())
	{
		Invalidate();
		UpdateWindow();
	}
}
