; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSMBIOSViewerDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SMBIOSViewer.h"

ClassCount=3
Class1=CSMBIOSViewerApp
Class2=CSMBIOSViewerDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_SMBIOSVIEWER_DIALOG

[CLS:CSMBIOSViewerApp]
Type=0
HeaderFile=SMBIOSViewer.h
ImplementationFile=SMBIOSViewer.cpp
Filter=N

[CLS:CSMBIOSViewerDlg]
Type=0
HeaderFile=SMBIOSViewerDlg.h
ImplementationFile=SMBIOSViewerDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CSMBIOSViewerDlg

[CLS:CAboutDlg]
Type=0
HeaderFile=SMBIOSViewerDlg.h
ImplementationFile=SMBIOSViewerDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_SMBIOSVIEWER_DIALOG]
Type=1
Class=CSMBIOSViewerDlg
ControlCount=7
Control1=IDC_STATIC,static,1342308352
Control2=IDC_COMBO_TABLE_TYPE,combobox,1344339971
Control3=IDC_LIST_RAW_DATA,SysListView32,1350665229
Control4=IDOK,button,1342242817
Control5=IDCANCEL,button,1342242816
Control6=IDC_STATIC,static,1342308352
Control7=IDC_EDIT_TABLE_INFO,edit,1350633604

