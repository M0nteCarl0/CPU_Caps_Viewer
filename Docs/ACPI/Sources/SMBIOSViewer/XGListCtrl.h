#pragma once
// XGListCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// XGListCtrl window

class XGListCtrl : public CListCtrl
{
// Construction
public:
	XGListCtrl();
private:
	CFont m_oFont;
	CBrush m_oBrush;
	CBrush m_oHighlightBrush;
	int m_nStart;
	int m_nEnd;
	int m_nCharWidth;
	int m_nNumCharsPerLine;

public:
	BOOL SetStartEnd(int nStart,int nEnd);
	void SetNumCharsPerLine(int nNumCharsPerLine);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(XGListCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~XGListCtrl();
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct );
	// Generated message map functions
protected:
	//{{AFX_MSG(XGListCtrl)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

