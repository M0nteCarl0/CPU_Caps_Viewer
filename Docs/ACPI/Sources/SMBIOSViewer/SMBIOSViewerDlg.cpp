// SMBIOSViewerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SMBIOSViewer.h"
#include "SMBIOSViewerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define		NUM_BYTES_PER_LINE				20

enum TableType
{
	Table_Type_0 = 0,
	Table_Type_1 = 1,
	Table_Type_2 = 2,
	Table_Type_3 = 3,
	Table_Type_4 = 4,
	Table_Type_11 = 11,
} ;
		
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSMBIOSViewerDlg dialog

CSMBIOSViewerDlg::CSMBIOSViewerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSMBIOSViewerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSMBIOSViewerDlg)
	m_szTableInfo = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSMBIOSViewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSMBIOSViewerDlg)
	DDX_Control(pDX, IDC_COMBO_TABLE_TYPE, m_oTableType);
	DDX_Control(pDX, IDC_LIST_RAW_DATA, m_oRawDataList);
	DDX_Text(pDX, IDC_EDIT_TABLE_INFO, m_szTableInfo);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSMBIOSViewerDlg, CDialog)
	//{{AFX_MSG_MAP(CSMBIOSViewerDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_CBN_SELCHANGE(IDC_COMBO_TABLE_TYPE, OnSelchangeComboTableType)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSMBIOSViewerDlg message handlers
void EnumTablesCallback(DWORD dwParam, EnumTableStruct* pstEnumTableStruct)
{
	CSMBIOSViewerDlg* pstDialog = (CSMBIOSViewerDlg*)dwParam;
	if(pstDialog)
	{
		pstDialog->AddTable(pstEnumTableStruct);
	}
}

BOOL CSMBIOSViewerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	CRect oRect;
	m_oRawDataList.GetClientRect(&oRect);
	m_oRawDataList.InsertColumn(0,_T("Raw Data"),LVCFMT_LEFT,oRect.Width());
	m_oRawDataList.SetNumCharsPerLine(NUM_BYTES_PER_LINE);

	if(m_oSMBIOSData.FetchSMBiosData())
	{
		m_oSMBIOSData.EnumTables((DWORD)this,EnumTablesCallback);
		DWORD dwBytesToOutput;
		DWORD dwFileSize = m_oSMBIOSData.GetRawDataLength();
		BYTE*	lpBaseAddress = m_oSMBIOSData.GetRawData();
		CString szLine;
		int nIndex = 0;

		dwBytesToOutput = ( dwFileSize >= NUM_BYTES_PER_LINE ) ? NUM_BYTES_PER_LINE : dwFileSize;

		while(dwBytesToOutput)
		{
			if(NUM_BYTES_PER_LINE == dwBytesToOutput)
			{
				szLine.Format(_T("%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X "),
					lpBaseAddress[0],
					lpBaseAddress[1],
					lpBaseAddress[2],
					lpBaseAddress[3],
					lpBaseAddress[4],
					lpBaseAddress[5],
					lpBaseAddress[6],
					lpBaseAddress[7],
					lpBaseAddress[8],
					lpBaseAddress[9],
					lpBaseAddress[10],
					lpBaseAddress[11],
					lpBaseAddress[12],
					lpBaseAddress[13],
					lpBaseAddress[14],
					lpBaseAddress[15],
					lpBaseAddress[16],
					lpBaseAddress[17],
					lpBaseAddress[18],
					lpBaseAddress[19]
					);
				m_oRawDataList.InsertItem(nIndex++,szLine,-1);
			}
			else
			{
				CString szChar;
				szLine.Empty();
				for(int j = 0 ; j < dwBytesToOutput; j++)
				{
					szChar.Format(_T("%02X "),lpBaseAddress[j]);
					szLine += szChar;
				}
				m_oRawDataList.InsertItem(nIndex++,szLine,-1);
			}
			
			lpBaseAddress += dwBytesToOutput;
			dwFileSize -= dwBytesToOutput;
			dwBytesToOutput = ( dwFileSize >= NUM_BYTES_PER_LINE ) ? NUM_BYTES_PER_LINE : dwFileSize;
		}

		//don't set any selection
		m_oTableType.SetCurSel(-1);
	}
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSMBIOSViewerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSMBIOSViewerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSMBIOSViewerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSMBIOSViewerDlg::OnSelchangeComboTableType() 
{
	// TODO: Add your control notification handler code here
	int nIndex = m_oTableType.GetCurSel();
	if(CB_ERR != nIndex)
	{
		CString szTemp;
		EnumTableStruct* pstTableStruct = (EnumTableStruct*)m_oTableType.GetItemData(nIndex);
		switch(pstTableStruct->dwTableType)
		{
		case Table_Type_0:
			{
				m_szTableInfo.Empty();

				m_szTableInfo = _T("#### BIOS Information ####\r\n\r\n");

				SMBios_TYPE0	stSMBiosType0;
				m_oSMBIOSData.GetData(stSMBiosType0);

				PopulateHeaderDetails((SMBios_TypeBase*)&stSMBiosType0,m_szTableInfo);
				
				if(stSMBiosType0.byVendor)
				{
					szTemp.Format(_T("Vendor = %s\r\n"),stSMBiosType0.szVendor);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType0.byBiosVersion)
				{
					szTemp.Format(_T("BIOS Version = %s\r\n"),stSMBiosType0.szBIOSVersion);
					m_szTableInfo += szTemp;
				}

				szTemp.Format(_T("BIOS Starting segment = %04XH\r\n"),stSMBiosType0.wBIOSStartingSegment);
				m_szTableInfo += szTemp;

				if(stSMBiosType0.byBIOSReleaseDate)
				{
					szTemp.Format(_T("BIOS Release Date = %s\r\n"),stSMBiosType0.szBIOSReleaseDate);
					m_szTableInfo += szTemp;
				}

				UpdateData(FALSE);
			}
			break;
		case Table_Type_1:
			{
				m_szTableInfo.Empty();

				m_szTableInfo = _T("#### System Information ####\r\n\r\n");

				SMBios_TYPE1	stSMBiosType1;
				m_oSMBIOSData.GetData(stSMBiosType1);

				PopulateHeaderDetails((SMBios_TypeBase*)&stSMBiosType1,m_szTableInfo);

				if(stSMBiosType1.byManufacturer)
				{
					szTemp.Format(_T("Manufacturer = %s\r\n"),stSMBiosType1.szManufacturer);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType1.byProductName)
				{
					szTemp.Format(_T("ProductName = %s\r\n"),stSMBiosType1.szProductName);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType1.byVersion)
				{
					szTemp.Format(_T("Version = %s\r\n"),stSMBiosType1.szVersion);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType1.bySerialNumber)
				{
					szTemp.Format(_T("SerialNumber = %s\r\n"),stSMBiosType1.szSerialNumber);
					m_szTableInfo += szTemp;
				}

				szTemp.Format(_T("WakeupType = %02XH\r\n"),stSMBiosType1.byWakeupType);
				m_szTableInfo += szTemp;

				if(stSMBiosType1.bySKUNumber)
				{
					szTemp.Format(_T("SKUNumber = %s\r\n"),stSMBiosType1.szSKUNumber);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType1.byFamily)
				{
					szTemp.Format(_T("Family = %s\r\n"),stSMBiosType1.szFamily);
					m_szTableInfo += szTemp;
				}
				UpdateData(FALSE);
			}
			break;

			case Table_Type_2:
			{
				m_szTableInfo.Empty();

				m_szTableInfo = _T("#### Base Board Information ####\r\n\r\n");

				SMBios_TYPE2	stSMBiosType2;
				m_oSMBIOSData.GetData(stSMBiosType2);

				PopulateHeaderDetails((SMBios_TypeBase*)&stSMBiosType2,m_szTableInfo);

				if(stSMBiosType2.byManufacturer)
				{
					szTemp.Format(_T("Manufacturer = %s\r\n"),stSMBiosType2.szManufacturer);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType2.byProductName)
				{
					szTemp.Format(_T("ProductName = %s\r\n"),stSMBiosType2.szProductName);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType2.byVersion)
				{
					szTemp.Format(_T("Version = %s\r\n"),stSMBiosType2.szVersion);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType2.bySerialNumber)
				{
					szTemp.Format(_T("SerialNumber = %s\r\n"),stSMBiosType2.szSerialNumber);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType2.byAssetTag)
				{
					szTemp.Format(_T("Asset Tag = %s\r\n"),stSMBiosType2.szAssetTag);
					m_szTableInfo += szTemp;
				}

				szTemp.Format(_T("Feature Flags = %02XH\r\n"),stSMBiosType2.byFeatureFlags);
				m_szTableInfo += szTemp;

				if(stSMBiosType2.byLocationInChassis)
				{
					szTemp.Format(_T("Location In Chassis = %s\r\n"),stSMBiosType2.szLocationInChassis);
					m_szTableInfo += szTemp;
				}

				szTemp.Format(_T("Chassis Handle = %04XH\r\n"),stSMBiosType2.wChassisHandle);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Board Type = %02XH\r\n"),stSMBiosType2.byBoardType);
				m_szTableInfo += szTemp;

				
				UpdateData(FALSE);
			}
			break;

		case Table_Type_3:
			{
				m_szTableInfo.Empty();

				m_szTableInfo = _T("#### System Enclosure or Chassis Type ####\r\n\r\n");

				SMBios_TYPE3	stSMBiosType3;
				m_oSMBIOSData.GetData(stSMBiosType3);

				PopulateHeaderDetails((SMBios_TypeBase*)&stSMBiosType3,m_szTableInfo);

				if(stSMBiosType3.byManufacturer)
				{
					szTemp.Format(_T("Manufacturer = %s\r\n"),stSMBiosType3.szManufacturer);
					m_szTableInfo += szTemp;
				}

				szTemp.Format(_T("Type = %02XH\r\n"),stSMBiosType3.byType);
				m_szTableInfo += szTemp;

				if(stSMBiosType3.byVersion)
				{
					szTemp.Format(_T("Version = %s\r\n"),stSMBiosType3.szVersion);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType3.bySerialNumber)
				{
					szTemp.Format(_T("SerialNumber = %s\r\n"),stSMBiosType3.szSerialNumber);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType3.byAssetTag)
				{
					szTemp.Format(_T("Asset Tag = %s\r\n"),stSMBiosType3.szAssetTag);
					m_szTableInfo += szTemp;
				}

				szTemp.Format(_T("Bootup State = %02XH\r\n"),stSMBiosType3.byBootupState);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Power Supply State = %02XH\r\n"),stSMBiosType3.byPowerSupplyState);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Thermal State = %02XH\r\n"),stSMBiosType3.byThermalState);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Security Status = %02XH\r\n"),stSMBiosType3.bySecurityStatus);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("OEM defined = %08XH\r\n"),stSMBiosType3.dwOEMdefined);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Height = %02XH\r\n"),stSMBiosType3.byHeight);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Number Of Power Cords = %02XH\r\n"),stSMBiosType3.byNumberOfPowerCords);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Contained Element Count = %02XH\r\n"),stSMBiosType3.byContainedElementCount);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Contained Element Record Length = %02XH\r\n"),stSMBiosType3.byContainedElementRecordLength);
				m_szTableInfo += szTemp;
				
				UpdateData(FALSE);
			}
			break;

		case Table_Type_4:
			{
				m_szTableInfo.Empty();

				m_szTableInfo = _T("#### Processor Information ####\r\n\r\n");

				SMBios_TYPE4	stSMBiosType4;
				m_oSMBIOSData.GetData(stSMBiosType4);

				PopulateHeaderDetails((SMBios_TypeBase*)&stSMBiosType4,m_szTableInfo);

				if(stSMBiosType4.bySocketDesignation)
				{
					szTemp.Format(_T("Socket Description = %s\r\n"),stSMBiosType4.szSocketDesignation);
					m_szTableInfo += szTemp;
				}

				szTemp.Format(_T("Processor Type = %02XH\r\n"),stSMBiosType4.byProcessorType);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Processor Family = %02XH\r\n"),stSMBiosType4.byProcessorFamily);
				m_szTableInfo += szTemp;

				if(stSMBiosType4.byProcessorManufacturer)
				{
					szTemp.Format(_T("Processer Manufacturer = %s\r\n"),stSMBiosType4.szProcessorManufacturer);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType4.byProcessorVersion)
				{
					szTemp.Format(_T("Processor Version = %s\r\n"),stSMBiosType4.szProcessorVersion);
					m_szTableInfo += szTemp;
				}

				szTemp.Format(_T("Voltage = %02XH\r\n"),stSMBiosType4.byVoltage);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("ExternalClock = %04XH\r\n"),stSMBiosType4.wExternalClock);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Max Speed = %04XH\r\n"),stSMBiosType4.wMaxSpeed);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Current Speed = %04XH\r\n"),stSMBiosType4.wCurrentSpeed);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Status = %02XH\r\n"),stSMBiosType4.byStatus);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("Processor Upgrade = %02XH\r\n"),stSMBiosType4.byProcessorUpgrade);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("L1 CacheHandle = %04XH\r\n"),stSMBiosType4.wL1CacheHandle);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("L2 CacheHandle = %04XH\r\n"),stSMBiosType4.wL2CacheHandle);
				m_szTableInfo += szTemp;

				szTemp.Format(_T("L3 CacheHandle = %04XH\r\n"),stSMBiosType4.wL3CacheHandle);
				m_szTableInfo += szTemp;

				if(stSMBiosType4.bySerialNumber)
				{
					szTemp.Format(_T("Serial Number = %s\r\n"),stSMBiosType4.szSerialNumber);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType4.byAssetTagNumber)
				{
					szTemp.Format(_T("Asset Tag Number = %s\r\n"),stSMBiosType4.byAssetTagNumber);
					m_szTableInfo += szTemp;
				}

				if(stSMBiosType4.byPartNumber)
				{
					szTemp.Format(_T("Part Number = %s\r\n"),stSMBiosType4.szPartNumber);
					m_szTableInfo += szTemp;
				}
				
				UpdateData(FALSE);
			}
			break;

		case Table_Type_11:
			{
				m_szTableInfo.Empty();

				m_szTableInfo = _T("#### OEM Strings ####\r\n\r\n");

				SMBios_TYPE11	stSMBiosType11;
				m_oSMBIOSData.GetData(stSMBiosType11);

				PopulateHeaderDetails((SMBios_TypeBase*)&stSMBiosType11,m_szTableInfo);

				szTemp.Format(_T("No. Of Strings = %d\r\n"),stSMBiosType11.byCountStrings);
				m_szTableInfo += szTemp;

				for(int j = 0 ; j < stSMBiosType11.byCountStrings; j++)
				{
					szTemp.Format(_T("String[%d] = %s\r\n"),j, stSMBiosType11.szStrings[j]);
					m_szTableInfo += szTemp;
				}

				UpdateData(FALSE);
			}
			break;

		default:
			m_szTableInfo.Empty();

			DWORD dwTableSize;
			SMBios_TypeBase* pstTypeBase = (SMBios_TypeBase*)m_oSMBIOSData.GetTableByIndex(pstTableStruct->dwIndex,dwTableSize);

			if(pstTypeBase)
			{
				PopulateHeaderDetails((SMBios_TypeBase*)pstTypeBase,m_szTableInfo);
			}

			UpdateData(FALSE);
			break;
		}

		m_oRawDataList.SetStartEnd(pstTableStruct->dwOffsetOfTableFromBeginning,pstTableStruct->dwOffsetOfTableFromBeginning + pstTableStruct->dwTableSize + 1);
		int nLineNo = pstTableStruct->dwOffsetOfTableFromBeginning/	NUM_BYTES_PER_LINE;
		m_oRawDataList.SetItemState(nLineNo,LVIF_STATE | LVIS_SELECTED | LVIS_FOCUSED,LVIS_SELECTED | LVIS_FOCUSED);
		m_oRawDataList.EnsureVisible(nLineNo,TRUE);

	}
}

void CSMBIOSViewerDlg::AddTable(EnumTableStruct* pstEnumTableStruct)
{
	EnumTableStruct* pstTableStruct = new EnumTableStruct;
	*pstTableStruct = *pstEnumTableStruct;
	CString szItem;
	szItem.Format(_T("Table Type = %d: Index = %d, Length = %d"),pstTableStruct->dwTableType,pstTableStruct->dwIndex,pstTableStruct->dwTableSize);
	int nIndex = m_oTableType.AddString(szItem);
	m_oTableType.SetItemData(nIndex,(DWORD)pstTableStruct);
}

void CSMBIOSViewerDlg::OnDestroy() 
{
	for(int j = 0 ; j < m_oTableType.GetCount(); j++)
		delete (EnumTableStruct*)m_oTableType.GetItemData(j);
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	
}

void CSMBIOSViewerDlg::PopulateHeaderDetails(SMBios_TypeBase* pstSMBios_TypeBase,CString& szText)
{
	CString szTemp;
	szTemp.Format(_T("Table = %d\r\n"),pstSMBios_TypeBase->stHeader.bySection);
	szText += szTemp;

	szTemp.Format(_T("Formatted Length = %d\r\n"),pstSMBios_TypeBase->stHeader.byLength);
	szText += szTemp;

	szTemp.Format(_T("Handle = %04XH\r\n"),pstSMBios_TypeBase->stHeader.wHandle);
	szText += szTemp;
}