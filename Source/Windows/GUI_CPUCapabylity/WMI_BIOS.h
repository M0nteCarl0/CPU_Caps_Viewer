#pragma once
using namespace System;
using namespace System::Management;
ref class WMI_BIOS
{
public:

	ManagementObjectSearcher^ WMI_BIOSS;
	ManagementObjectCollection^ WMI_BIOSProperties;
	String^ Manufacture;
	String^ Version;
	int ReleaseYear;
	int ReleaseMonth;
	int ReleaseDay;
	//DateTime^ Release;
	
	WMI_BIOS(void)
	{
		WMI_BIOSS = gcnew  ManagementObjectSearcher("root\\CIMV2",
			"SELECT * FROM  Win32_BIOS");
	WMI_BIOSProperties = WMI_BIOSS->Get();

	String^ ReleaseDate;
	String^ Year;
	String^Month;
	String^ Days;
	
		for each (ManagementObject^ queryObj in WMI_BIOSProperties)
		{
			Manufacture = queryObj["Manufacturer"]->ToString();
			Version =  queryObj["SMBIOSBIOSVersion"]->ToString();
	
			ReleaseDate = queryObj["ReleaseDate"]->ToString();
			
			Year = ReleaseDate->Substring(0, 4);
			Month = ReleaseDate->Substring(4, 2);
			Days = ReleaseDate->Substring(6, 2);
	
			ReleaseYear = int::Parse(Year);
			ReleaseMonth = int::Parse(Month);
			ReleaseDay = int::Parse(Days);

		};
		
	}
};

