#pragma once
using namespace System::Management;
ref class WMI_CPU
{
public:
	ManagementObjectSearcher^ WMI_CPUS;
	ManagementOperationObserver^ results;
	ManagementObjectCollection^ CPUProperties;
	Object^ AsyncObj;
	int CurrentClock;
	int MaiximumClock;
	int BCLK;
	bool Ready;

	void GetPropertyAsync(void)
	{
		Ready = false;
		WMI_CPUS->Get(results);
	}
	WMI_CPU(void)
	{
		WMI_CPUS = gcnew  ManagementObjectSearcher("root\\CIMV2",
			"SELECT * FROM Win32_Processor");
		CPUProperties = WMI_CPUS->Get();
		results = gcnew
			ManagementOperationObserver();

		// Attach handler to events for results and completion
		results->ObjectReady += gcnew
			ObjectReadyEventHandler(this, &WMI_CPU::Done);
		//results->Completed += gcnew
			//CompletedEventHandler(this, &MainView::Complete);


		for each (ManagementObject^ queryObj in CPUProperties)
		{
			MaiximumClock = int::Parse(queryObj["MaxClockSpeed"]->ToString());
			CurrentClock = int::Parse(queryObj["CurrentClockSpeed"]->ToString());
			BCLK = int::Parse(queryObj["ExtClock"]->ToString());
		};

	}

	 System::Void Done(System::Object^sender,
		System::Management::ObjectReadyEventArgs^ obj)
	{
		AsyncObj = obj->NewObject["CurrentClockSpeed"];
		CurrentClock = int::Parse(AsyncObj->ToString());
		Ready = true;
	}
	
};

