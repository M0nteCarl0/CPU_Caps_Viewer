#pragma once
#include <Windows.h>
#include <OlsApi.h>
#include "CPUInformation.h"
#include "SupportedInstructionsForm.h"
#include "WMI_CPU.h"
#include "WMI_Motheboard.h"
#include <math.h>
#include "WMI_BIOS.h"

namespace GUI_CPUCapabylity {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Management;
	/// <summary>
	/// ������ ��� Form1
	/// </summary>
	public ref class MainView : public System::Windows::Forms::Form
	{
	public:
    CPUInformation* _Info;
	//SpectreMeldtownSecurityCheck* _SpectreMeldtownSecurityCheck;
	private: System::Windows::Forms::Label^  CPU_Vendor;
	public: 

	private: System::Windows::Forms::Label^  label50;
	private: System::Windows::Forms::Label^  CPU_CoreCount;
	private: System::Windows::Forms::Label^  CPU_ThreadCount;
	private: System::Windows::Forms::Label^  CPU_BCLK;



	private: System::Windows::Forms::Label^  CPU_Current_Clock;
	private: System::Windows::Forms::Label^  CPU_Maximum_Clock;


	private: System::Windows::Forms::Label^  L3_Ways_Count;

	private: System::Windows::Forms::Label^  L2_Ways_Count;

	private: System::Windows::Forms::Label^  L1D_Ways_Count;

	private: System::Windows::Forms::Label^  L1i_Ways_Count;
	private: System::Windows::Forms::Label^  CPU_Level3_topology;


	private: System::Windows::Forms::Label^  CPU_Level2_topology;

	private: System::Windows::Forms::Label^  CPU_Level1D_topology;

	private: System::Windows::Forms::Label^  CPU_Level1I_topology;
	private: System::Windows::Forms::Label^  SupportedInstructionsLabel;


	private: System::Windows::Forms::Label^  CPU_Revision;

	private: System::Windows::Forms::Label^  CPU_Stepping;

	private: System::Windows::Forms::Label^  CPU_ExtendedModel;

	private: System::Windows::Forms::Label^  CPU_Model;

	private: System::Windows::Forms::Label^  CPU_ExtendedFamily;

	private: System::Windows::Forms::Label^  CPU_Family;

	private: System::Windows::Forms::Label^  CPU_Brand;
	private: System::Windows::Forms::Label^  label30;
	private: System::Windows::Forms::Label^  Caches_L1i_Cores;

	private: System::Windows::Forms::Label^  Caches_L1i_Size;

	private: System::Windows::Forms::Label^  label40;
	private: System::Windows::Forms::Label^  Caches_L3_Size;

	private: System::Windows::Forms::Label^  label38;
	private: System::Windows::Forms::Label^  Caches_L2_Cores;

	private: System::Windows::Forms::Label^  Caches_L2_Size;

	private: System::Windows::Forms::Label^  label34;
	private: System::Windows::Forms::Label^  label33;
	private: System::Windows::Forms::Label^  Caches_L1D_Cores;

	private: System::Windows::Forms::Label^  Caches_L1D_Size;
	private: System::Windows::Forms::GroupBox^  groupBox8;
	private: System::Windows::Forms::Label^  Motheboard_Model;

	private: System::Windows::Forms::Label^  Motheboard_Manufacture;

	private: System::Windows::Forms::Label^  label32;
	private: System::Windows::Forms::Label^  label35;
	private: System::Windows::Forms::Label^  Motheboard_Revision;
	private: System::Windows::Forms::GroupBox^  UEFI_BIOS;
	private: System::Windows::Forms::Label^  UEFI_BIOS_Version;




	private: System::Windows::Forms::Label^  UEFI_BIOS_Manufacture;

	private: System::Windows::Forms::Label^  label42;
	private: System::Windows::Forms::Label^  label44;
	private: System::Windows::Forms::Label^  UEFI_BIOS_Date;

	private: System::Windows::Forms::Label^  label43;
	private: System::Windows::Forms::Label^  Motheboard_Serial;

	private: System::Windows::Forms::Label^  label46;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  overclockTunnerToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  intelSAIOVoltageCalculatorToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  cPUPowerConsumationCalculatorToolStripMenuItem;



	public:
	    WMI_CPU^		_WMI_CPU;
		WMI_Motheboard^ _WMI_Motheboard;
		WMI_BIOS^ _WMI_BIOS;
private: System::Windows::Forms::TabPage^  tabPage6;
public: 
private: System::Windows::Forms::GroupBox^  groupBox9;


private: System::Windows::Forms::GroupBox^  groupBox10;
private: System::Windows::Forms::CheckBox^  KVAShadowRequired;
private: System::Windows::Forms::CheckBox^  BTIHardwarePresent;


private: System::Windows::Forms::CheckBox^  BTIWindowsSupportPresent;

private: System::Windows::Forms::CheckBox^  BTIDisabledBySystemPolicy;
private: System::Windows::Forms::CheckBox^  BTIWindowsSupportEnabled;


private: System::Windows::Forms::CheckBox^  BTIDisabledByNoHardwareSupport;

private: System::Windows::Forms::CheckBox^  KVAShadowPcidEnabled;
private: System::Windows::Forms::CheckBox^  KVAShadowWindowsSupportEnabled;
private: System::Windows::Forms::CheckBox^  KVAShadowWindowsSupportPresent;
private: System::Windows::Forms::Label^  uCodeVersion;

private: System::Windows::Forms::Label^  label28;
private: System::Windows::Forms::StatusStrip^  statusStrip1;
private: System::Windows::Forms::ToolStripStatusLabel^  CPUVoltage;
private: System::Windows::Forms::ToolStripStatusLabel^  TemperatureCPU;
private: System::Windows::Forms::ToolStripStatusLabel^  CPU_Power_Package;

		 float RAPL_Power_Units;
		 float Energy_Power_Units;

		 SupportedInstructionsForm^ _SupportedInstructionsForm;
	public:	MainView(void)
		{
		InitializeComponent();
		bool Flag = 	InitializeOls();
		

			_SupportedInstructionsForm = gcnew  SupportedInstructionsForm();
			PollingCPUFrequncy->Enabled = false;
            _Info					   = new CPUInformation();
            CPU_Vendor->Text		   = gcnew String( _Info->GetVendorname().c_str());
            CPU_Brand->Text			   = gcnew String( _Info->GetBrandName().c_str());
			CPU_Family->Text		   = gcnew String( _Info->GetFamilyCode().ToString());
			CPU_ExtendedFamily->Text   = gcnew String(_Info->GetExtendeFamily().ToString());
			CPU_Model->Text            = String::Format("{0:X}",_Info->GetModel());
            CPU_ExtendedModel->Text    = gcnew String(_Info->GetExtendedModel().ToString());
			CPU_Stepping->Text		   = gcnew String(_Info->GetStepingID().ToString());
			CPU_Revision->Text         = String::Format("{0}",_Info->GetStepingID());
			CPU_CoreCount->Text        = String::Format("{0}",_Info->GetPhysicalCPUCount());
			CPU_ThreadCount->Text      = String::Format("{0}",_Info->GetLogicalCPUCount());

			CPU_Level1I_topology->Text =  String::Format("{0} X {1}KB",_Info->GetPhysicalCPUCount(), _Info->GetCacheSize(0)/1024 );
			Caches_L1i_Size->Text = String::Format("{0}KB",_Info->GetCacheSize(0)/1024 );
			L1i_Ways_Count->Text = String::Format("{0}-way", _Info->GetCacheWaysAssociativity(0));
			Caches_L1i_Cores->Text = String::Format("X {0}",_Info->GetPhysicalCPUCount());
			 

            CPU_Level1D_topology->Text =  String::Format("{0} X {1}KB",_Info->GetPhysicalCPUCount(),_Info->GetCacheSize(1)/1024);
			L1D_Ways_Count->Text  = String::Format("{0}-way", _Info->GetCacheWaysAssociativity(1));
			Caches_L1D_Size->Text =  String::Format("{0}KB",_Info->GetCacheSize(1)/1024 );
			Caches_L1D_Cores->Text = String::Format("X {0}",_Info->GetPhysicalCPUCount());
			if(_Info->GetMaximumCacheLevel()>1)
			{
				CPU_Level2_topology->Text  =  String::Format("{0} X {1}KB",_Info->GetPhysicalCPUCount(),_Info->GetCacheSize(2)/1024);
				L2_Ways_Count->Text  = String::Format("{0}-way", _Info->GetCacheWaysAssociativity(2));
				Caches_L2_Size->Text = String::Format("{0}KB",_Info->GetCacheSize(2)/1024 );
				Caches_L2_Cores->Text = String::Format("X {0}",_Info->GetPhysicalCPUCount());
				
			}
			if(_Info->GetMaximumCacheLevel()>2)
			{
				CPU_Level3_topology->Text  =  String::Format("{0}MB",_Info->GetCacheSize(3)/(1024*1024));
				L3_Ways_Count->Text  =  String::Format("{0}-way",_Info->GetCacheWaysAssociativity(3));
				Caches_L3_Size->Text = String::Format("{0}MB",_Info->GetCacheSize(3)/(1024*1024) );
				
			}

			size_t Usage1CoreMaxAratio = 0;
			size_t Usage2CoreMaxAratio = 0;
			size_t Usage3CoreMaxAratio = 0;
			size_t Usage4CoreMaxAratio = 0;

			size_t Usage5CoreMaxAratio = 0;
			size_t Usage6CoreMaxAratio = 0;
			size_t uCodeVersion;
			size_t BCLK = 0;
			size_t PlatformIDl = 0;
			size_t ThermalTDP = 0;
			size_t MaximumThermalTDP = 0;
			
			float CoreVoltage = 0;
			size_t FIVR_Setup;
			float FIVRVoltage;
			
			if (Flag)
			{
				DWORD L = 0;
				DWORD H =0;

#if 0
				if (Rdmsr(0x17, &L, &H))
				{
					TDP = (L & 0x3FF) * 0.125 ;


				}
#endif

#if 0
				if (Wrmsr(0x150,0, 0x80000012) && Rdmsr(0x150, &L, &H))
				{
					FIVRVoltage = (float)H * 0.005;
				}

				L = 0;
				H = 0;
				if (RdmsrTx(0x10A, &L, &H,0x1))
				{
					bool Flag;


				}
#endif				

				if (Rdmsr(0x606, &L, &H))
				{

					DWORD Base = L & 0xF;
					RAPL_Power_Units = 1 / powf(2.0, (float)Base);
					Base = (L >> 8) & 0xF;
					Energy_Power_Units = 1 / powf(2.0, (float)Base);


				}


				if (Rdmsr(0x614, &L, &H))
				{
					MaximumThermalTDP = (H & 0x7FF) * RAPL_Power_Units;
					ThermalTDP = (L & 0x7FF)  * RAPL_Power_Units;


				}



				if (Rdmsr(0x17, &L, NULL))
				{
					PlatformIDl = L;

				
				}


				if (Rdmsr(0x61E, &L, NULL))
				{
					BCLK = L;


				}

				L = 0;
				//if (Rdmsr(0x198, &L, &H))
				//{
				//	DWORD Extracted = L >> 32;
				//	CoreVoltage = (float)((L >> 32) & 0x7FF) * ((float)1 / pow(2.0, 13));
					//CoreVoltage+= (float)((H >> 32) & 0x7FF) * ((float)1 / pow(2.0, 13));

			//	}

				L = 0;
				if (Rdmsr(0x1AD, &L, &H))
				{
					DWORD Result = L;

					Usage1CoreMaxAratio = L &0xFF;
					Usage2CoreMaxAratio = ((L >>8) & 0xFF);
					Usage3CoreMaxAratio = (L >> 16) & 0xFF;
					Usage4CoreMaxAratio = (L >> 32) & 0xFF;
				}

				L = 0;
				H = 0;
				if (RdmsrPx(0x8B, &L, &H, 0x1))
				{
					uCodeVersion = L | H<<32;
					this->uCodeVersion->Text = String::Format("0X{0:X}", uCodeVersion);
				}
			}



			Init_WMI_CPU_Handle();
			Init_WMI_MotheboardAndBIOS_Handle();
			Security_Check_Handle();
			PollingCPUFrequncy->Enabled = true;
			//
			//TODO: �������� ��� ������������
			//
		}

		void Security_Check_Handle(void)
		{
			//_SpectreMeldtownSecurityCheck = new SpectreMeldtownSecurityCheck();

		}

		void Init_WMI_CPU_Handle(void)
		{
			 
			 _WMI_CPU = gcnew WMI_CPU();
			 CPU_Current_Clock->Text = String::Format("{0}Mhz", _WMI_CPU->CurrentClock);
			 CPU_Maximum_Clock->Text = String::Format("{0}Mhz", _WMI_CPU->MaiximumClock);
			 CPU_BCLK->Text = String::Format("{0}Mhz", _WMI_CPU->BCLK);
				  
		}

		void Init_WMI_MotheboardAndBIOS_Handle(void)
		{
			_WMI_Motheboard = gcnew WMI_Motheboard();

			Motheboard_Manufacture->Text=_WMI_Motheboard->Manufacture;
			Motheboard_Model->Text=_WMI_Motheboard->Model;
			Motheboard_Serial->Text=_WMI_Motheboard->Serial;	  
			Motheboard_Revision->Text=_WMI_Motheboard->Revision;

			_WMI_BIOS=gcnew WMI_BIOS();
			UEFI_BIOS_Manufacture->Text =_WMI_BIOS->Manufacture;
            UEFI_BIOS_Version->Text = _WMI_BIOS-> Version;
			UEFI_BIOS_Date->Text = String::Format("{0}/{1}/{2}", _WMI_BIOS->ReleaseMonth, _WMI_BIOS->ReleaseDay, _WMI_BIOS->ReleaseYear);
		}


		void Init_WMI_DRAM_Handle(void)
		{




		}

	protected:
			
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MainView()
		{
			delete _Info;
			DeinitializeOls();
			if (components)
			{
				delete components;
			}
		}
    private: System::Windows::Forms::TabControl^  tabControl1;
    protected: 

    private: System::Windows::Forms::TabPage^  tabPage2;
    private: System::Windows::Forms::TabPage^  tabPage3;
    private: System::Windows::Forms::TabPage^  tabPage4;


















	private: System::Windows::Forms::GroupBox^  groupBox1;




	private: System::Windows::Forms::Label^  label11;


	private: System::Windows::Forms::Label^  label10;

	private: System::Windows::Forms::GroupBox^  groupBox3;



	private: System::Windows::Forms::Label^  label14;


	private: System::Windows::Forms::Label^  label15;

	private: System::Windows::Forms::GroupBox^  groupBox2;



	private: System::Windows::Forms::Label^  label12;


	private: System::Windows::Forms::Label^  label13;

	private: System::Windows::Forms::GroupBox^  groupBox4;



	private: System::Windows::Forms::Label^  label16;


	private: System::Windows::Forms::Label^  label17;

	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::GroupBox^  groupBox5;




	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label8;




	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label5;


	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label6;




	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label7;




	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::GroupBox^  groupBox6;
	private: System::Windows::Forms::GroupBox^  groupBox7;





	private: System::Windows::Forms::Label^  label21;








	private: System::Windows::Forms::Label^  label20;


	private: System::Windows::Forms::Label^  label19;





	private: System::Windows::Forms::Label^  label18;



private: System::Windows::Forms::Label^  label24;



private: System::Windows::Forms::Label^  label23;




private: System::Windows::Forms::Label^  label22;
private: System::Windows::Forms::MenuStrip^  menuStrip1;
private: System::Windows::Forms::Label^  label27;


private: System::Windows::Forms::Label^  label26;


private: System::Windows::Forms::Label^  label25;


private: System::Windows::Forms::TabPage^  tabPage5;
private: System::Windows::Forms::Timer^  PollingCPUFrequncy;
private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->label50 = (gcnew System::Windows::Forms::Label());
			this->CPU_CoreCount = (gcnew System::Windows::Forms::Label());
			this->CPU_ThreadCount = (gcnew System::Windows::Forms::Label());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->CPU_BCLK = (gcnew System::Windows::Forms::Label());
			this->CPU_Current_Clock = (gcnew System::Windows::Forms::Label());
			this->CPU_Maximum_Clock = (gcnew System::Windows::Forms::Label());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->L3_Ways_Count = (gcnew System::Windows::Forms::Label());
			this->L2_Ways_Count = (gcnew System::Windows::Forms::Label());
			this->L1D_Ways_Count = (gcnew System::Windows::Forms::Label());
			this->L1i_Ways_Count = (gcnew System::Windows::Forms::Label());
			this->CPU_Level3_topology = (gcnew System::Windows::Forms::Label());
			this->CPU_Level2_topology = (gcnew System::Windows::Forms::Label());
			this->CPU_Level1D_topology = (gcnew System::Windows::Forms::Label());
			this->CPU_Level1I_topology = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->uCodeVersion = (gcnew System::Windows::Forms::Label());
			this->label28 = (gcnew System::Windows::Forms::Label());
			this->SupportedInstructionsLabel = (gcnew System::Windows::Forms::Label());
			this->CPU_Revision = (gcnew System::Windows::Forms::Label());
			this->CPU_Stepping = (gcnew System::Windows::Forms::Label());
			this->CPU_ExtendedModel = (gcnew System::Windows::Forms::Label());
			this->CPU_Model = (gcnew System::Windows::Forms::Label());
			this->CPU_ExtendedFamily = (gcnew System::Windows::Forms::Label());
			this->CPU_Family = (gcnew System::Windows::Forms::Label());
			this->CPU_Brand = (gcnew System::Windows::Forms::Label());
			this->CPU_Vendor = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->label40 = (gcnew System::Windows::Forms::Label());
			this->Caches_L3_Size = (gcnew System::Windows::Forms::Label());
			this->label38 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->Caches_L2_Cores = (gcnew System::Windows::Forms::Label());
			this->Caches_L2_Size = (gcnew System::Windows::Forms::Label());
			this->label34 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->label33 = (gcnew System::Windows::Forms::Label());
			this->Caches_L1D_Cores = (gcnew System::Windows::Forms::Label());
			this->Caches_L1D_Size = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->Caches_L1i_Cores = (gcnew System::Windows::Forms::Label());
			this->Caches_L1i_Size = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
			this->UEFI_BIOS = (gcnew System::Windows::Forms::GroupBox());
			this->label44 = (gcnew System::Windows::Forms::Label());
			this->UEFI_BIOS_Date = (gcnew System::Windows::Forms::Label());
			this->UEFI_BIOS_Version = (gcnew System::Windows::Forms::Label());
			this->UEFI_BIOS_Manufacture = (gcnew System::Windows::Forms::Label());
			this->label42 = (gcnew System::Windows::Forms::Label());
			this->label43 = (gcnew System::Windows::Forms::Label());
			this->groupBox8 = (gcnew System::Windows::Forms::GroupBox());
			this->Motheboard_Serial = (gcnew System::Windows::Forms::Label());
			this->label46 = (gcnew System::Windows::Forms::Label());
			this->Motheboard_Revision = (gcnew System::Windows::Forms::Label());
			this->Motheboard_Model = (gcnew System::Windows::Forms::Label());
			this->Motheboard_Manufacture = (gcnew System::Windows::Forms::Label());
			this->label32 = (gcnew System::Windows::Forms::Label());
			this->label35 = (gcnew System::Windows::Forms::Label());
			this->tabPage5 = (gcnew System::Windows::Forms::TabPage());
			this->tabPage4 = (gcnew System::Windows::Forms::TabPage());
			this->tabPage6 = (gcnew System::Windows::Forms::TabPage());
			this->groupBox10 = (gcnew System::Windows::Forms::GroupBox());
			this->KVAShadowPcidEnabled = (gcnew System::Windows::Forms::CheckBox());
			this->KVAShadowWindowsSupportEnabled = (gcnew System::Windows::Forms::CheckBox());
			this->KVAShadowWindowsSupportPresent = (gcnew System::Windows::Forms::CheckBox());
			this->KVAShadowRequired = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox9 = (gcnew System::Windows::Forms::GroupBox());
			this->BTIDisabledByNoHardwareSupport = (gcnew System::Windows::Forms::CheckBox());
			this->BTIDisabledBySystemPolicy = (gcnew System::Windows::Forms::CheckBox());
			this->BTIWindowsSupportEnabled = (gcnew System::Windows::Forms::CheckBox());
			this->BTIWindowsSupportPresent = (gcnew System::Windows::Forms::CheckBox());
			this->BTIHardwarePresent = (gcnew System::Windows::Forms::CheckBox());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->toolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->overclockTunnerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->intelSAIOVoltageCalculatorToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->cPUPowerConsumationCalculatorToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->PollingCPUFrequncy = (gcnew System::Windows::Forms::Timer(this->components));
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->CPUVoltage = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->TemperatureCPU = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->CPU_Power_Package = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->tabControl1->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->groupBox7->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->tabPage2->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->tabPage3->SuspendLayout();
			this->UEFI_BIOS->SuspendLayout();
			this->groupBox8->SuspendLayout();
			this->tabPage6->SuspendLayout();
			this->groupBox10->SuspendLayout();
			this->groupBox9->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			this->statusStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Controls->Add(this->tabPage3);
			this->tabControl1->Controls->Add(this->tabPage5);
			this->tabControl1->Controls->Add(this->tabPage4);
			this->tabControl1->Controls->Add(this->tabPage6);
			this->tabControl1->Location = System::Drawing::Point(0, 29);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->ShowToolTips = true;
			this->tabControl1->Size = System::Drawing::Size(433, 368);
			this->tabControl1->TabIndex = 0;
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->label50);
			this->tabPage1->Controls->Add(this->CPU_CoreCount);
			this->tabPage1->Controls->Add(this->CPU_ThreadCount);
			this->tabPage1->Controls->Add(this->label24);
			this->tabPage1->Controls->Add(this->label23);
			this->tabPage1->Controls->Add(this->label22);
			this->tabPage1->Controls->Add(this->groupBox7);
			this->tabPage1->Controls->Add(this->groupBox6);
			this->tabPage1->Controls->Add(this->groupBox5);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(425, 342);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"CPU";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// label50
			// 
			this->label50->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label50->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->label50->Location = System::Drawing::Point(87, 292);
			this->label50->Name = L"label50";
			this->label50->Size = System::Drawing::Size(25, 20);
			this->label50->TabIndex = 37;
			this->label50->Text = L" ";
			this->label50->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			this->label50->Visible = false;
			// 
			// CPU_CoreCount
			// 
			this->CPU_CoreCount->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_CoreCount->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_CoreCount->Location = System::Drawing::Point(186, 293);
			this->CPU_CoreCount->Name = L"CPU_CoreCount";
			this->CPU_CoreCount->Size = System::Drawing::Size(25, 20);
			this->CPU_CoreCount->TabIndex = 36;
			this->CPU_CoreCount->Text = L" ";
			this->CPU_CoreCount->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_ThreadCount
			// 
			this->CPU_ThreadCount->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_ThreadCount->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_ThreadCount->Location = System::Drawing::Point(264, 292);
			this->CPU_ThreadCount->Name = L"CPU_ThreadCount";
			this->CPU_ThreadCount->Size = System::Drawing::Size(24, 20);
			this->CPU_ThreadCount->TabIndex = 35;
			this->CPU_ThreadCount->Text = L" ";
			this->CPU_ThreadCount->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Location = System::Drawing::Point(8, 297);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(73, 13);
			this->label24->TabIndex = 31;
			this->label24->Text = L"Modules/CCX";
			this->label24->Visible = false;
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(153, 296);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(34, 13);
			this->label23->TabIndex = 29;
			this->label23->Text = L"Cores";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(217, 296);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(46, 13);
			this->label22->TabIndex = 27;
			this->label22->Text = L"Threads";
			// 
			// groupBox7
			// 
			this->groupBox7->Controls->Add(this->CPU_BCLK);
			this->groupBox7->Controls->Add(this->CPU_Current_Clock);
			this->groupBox7->Controls->Add(this->CPU_Maximum_Clock);
			this->groupBox7->Controls->Add(this->label27);
			this->groupBox7->Controls->Add(this->label26);
			this->groupBox7->Controls->Add(this->label25);
			this->groupBox7->Location = System::Drawing::Point(6, 162);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Size = System::Drawing::Size(155, 122);
			this->groupBox7->TabIndex = 20;
			this->groupBox7->TabStop = false;
			this->groupBox7->Text = L"Clocks";
			// 
			// CPU_BCLK
			// 
			this->CPU_BCLK->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_BCLK->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_BCLK->Location = System::Drawing::Point(76, 73);
			this->CPU_BCLK->Name = L"CPU_BCLK";
			this->CPU_BCLK->Size = System::Drawing::Size(74, 20);
			this->CPU_BCLK->TabIndex = 23;
			this->CPU_BCLK->Text = L" ";
			this->CPU_BCLK->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_Current_Clock
			// 
			this->CPU_Current_Clock->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Current_Clock->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Current_Clock->Location = System::Drawing::Point(76, 48);
			this->CPU_Current_Clock->Name = L"CPU_Current_Clock";
			this->CPU_Current_Clock->Size = System::Drawing::Size(74, 20);
			this->CPU_Current_Clock->TabIndex = 22;
			this->CPU_Current_Clock->Text = L" ";
			this->CPU_Current_Clock->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_Maximum_Clock
			// 
			this->CPU_Maximum_Clock->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Maximum_Clock->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Maximum_Clock->Location = System::Drawing::Point(76, 21);
			this->CPU_Maximum_Clock->Name = L"CPU_Maximum_Clock";
			this->CPU_Maximum_Clock->Size = System::Drawing::Size(74, 20);
			this->CPU_Maximum_Clock->TabIndex = 21;
			this->CPU_Maximum_Clock->Text = L" ";
			this->CPU_Maximum_Clock->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(7, 74);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(34, 13);
			this->label27->TabIndex = 8;
			this->label27->Text = L"BCLK";
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Location = System::Drawing::Point(7, 48);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(41, 13);
			this->label26->TabIndex = 6;
			this->label26->Text = L"Current";
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(7, 22);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(51, 13);
			this->label25->TabIndex = 4;
			this->label25->Text = L"Maximum";
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->L3_Ways_Count);
			this->groupBox6->Controls->Add(this->L2_Ways_Count);
			this->groupBox6->Controls->Add(this->L1D_Ways_Count);
			this->groupBox6->Controls->Add(this->L1i_Ways_Count);
			this->groupBox6->Controls->Add(this->CPU_Level3_topology);
			this->groupBox6->Controls->Add(this->CPU_Level2_topology);
			this->groupBox6->Controls->Add(this->CPU_Level1D_topology);
			this->groupBox6->Controls->Add(this->CPU_Level1I_topology);
			this->groupBox6->Controls->Add(this->label21);
			this->groupBox6->Controls->Add(this->label20);
			this->groupBox6->Controls->Add(this->label19);
			this->groupBox6->Controls->Add(this->label18);
			this->groupBox6->Location = System::Drawing::Point(167, 162);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Size = System::Drawing::Size(177, 122);
			this->groupBox6->TabIndex = 19;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = L"Caches";
			// 
			// L3_Ways_Count
			// 
			this->L3_Ways_Count->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->L3_Ways_Count->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->L3_Ways_Count->Location = System::Drawing::Point(123, 94);
			this->L3_Ways_Count->Name = L"L3_Ways_Count";
			this->L3_Ways_Count->Size = System::Drawing::Size(48, 22);
			this->L3_Ways_Count->TabIndex = 34;
			this->L3_Ways_Count->Text = L" ";
			this->L3_Ways_Count->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// L2_Ways_Count
			// 
			this->L2_Ways_Count->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->L2_Ways_Count->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->L2_Ways_Count->Location = System::Drawing::Point(123, 71);
			this->L2_Ways_Count->Name = L"L2_Ways_Count";
			this->L2_Ways_Count->Size = System::Drawing::Size(48, 22);
			this->L2_Ways_Count->TabIndex = 33;
			this->L2_Ways_Count->Text = L" ";
			this->L2_Ways_Count->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// L1D_Ways_Count
			// 
			this->L1D_Ways_Count->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->L1D_Ways_Count->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->L1D_Ways_Count->Location = System::Drawing::Point(123, 46);
			this->L1D_Ways_Count->Name = L"L1D_Ways_Count";
			this->L1D_Ways_Count->Size = System::Drawing::Size(48, 22);
			this->L1D_Ways_Count->TabIndex = 32;
			this->L1D_Ways_Count->Text = L" ";
			this->L1D_Ways_Count->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// L1i_Ways_Count
			// 
			this->L1i_Ways_Count->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->L1i_Ways_Count->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->L1i_Ways_Count->Location = System::Drawing::Point(123, 19);
			this->L1i_Ways_Count->Name = L"L1i_Ways_Count";
			this->L1i_Ways_Count->Size = System::Drawing::Size(48, 22);
			this->L1i_Ways_Count->TabIndex = 31;
			this->L1i_Ways_Count->Text = L" ";
			this->L1i_Ways_Count->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_Level3_topology
			// 
			this->CPU_Level3_topology->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Level3_topology->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Level3_topology->Location = System::Drawing::Point(47, 96);
			this->CPU_Level3_topology->Name = L"CPU_Level3_topology";
			this->CPU_Level3_topology->Size = System::Drawing::Size(70, 20);
			this->CPU_Level3_topology->TabIndex = 30;
			this->CPU_Level3_topology->Text = L" ";
			this->CPU_Level3_topology->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_Level2_topology
			// 
			this->CPU_Level2_topology->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Level2_topology->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Level2_topology->Location = System::Drawing::Point(47, 71);
			this->CPU_Level2_topology->Name = L"CPU_Level2_topology";
			this->CPU_Level2_topology->Size = System::Drawing::Size(70, 20);
			this->CPU_Level2_topology->TabIndex = 29;
			this->CPU_Level2_topology->Text = L" ";
			this->CPU_Level2_topology->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_Level1D_topology
			// 
			this->CPU_Level1D_topology->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Level1D_topology->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Level1D_topology->Location = System::Drawing::Point(47, 45);
			this->CPU_Level1D_topology->Name = L"CPU_Level1D_topology";
			this->CPU_Level1D_topology->Size = System::Drawing::Size(70, 20);
			this->CPU_Level1D_topology->TabIndex = 28;
			this->CPU_Level1D_topology->Text = L" ";
			this->CPU_Level1D_topology->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_Level1I_topology
			// 
			this->CPU_Level1I_topology->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Level1I_topology->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Level1I_topology->Location = System::Drawing::Point(47, 19);
			this->CPU_Level1I_topology->Name = L"CPU_Level1I_topology";
			this->CPU_Level1I_topology->Size = System::Drawing::Size(70, 20);
			this->CPU_Level1I_topology->TabIndex = 27;
			this->CPU_Level1I_topology->Text = L" ";
			this->CPU_Level1I_topology->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(6, 99);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(19, 13);
			this->label21->TabIndex = 24;
			this->label21->Text = L"L3";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(6, 74);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(19, 13);
			this->label20->TabIndex = 21;
			this->label20->Text = L"L2";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(6, 48);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(27, 13);
			this->label19->TabIndex = 13;
			this->label19->Text = L"L1D";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(6, 22);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(22, 13);
			this->label18->TabIndex = 10;
			this->label18->Text = L"L1I";
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->uCodeVersion);
			this->groupBox5->Controls->Add(this->label28);
			this->groupBox5->Controls->Add(this->SupportedInstructionsLabel);
			this->groupBox5->Controls->Add(this->CPU_Revision);
			this->groupBox5->Controls->Add(this->CPU_Stepping);
			this->groupBox5->Controls->Add(this->CPU_ExtendedModel);
			this->groupBox5->Controls->Add(this->CPU_Model);
			this->groupBox5->Controls->Add(this->CPU_ExtendedFamily);
			this->groupBox5->Controls->Add(this->CPU_Family);
			this->groupBox5->Controls->Add(this->CPU_Brand);
			this->groupBox5->Controls->Add(this->CPU_Vendor);
			this->groupBox5->Controls->Add(this->label9);
			this->groupBox5->Controls->Add(this->label8);
			this->groupBox5->Controls->Add(this->label1);
			this->groupBox5->Controls->Add(this->label5);
			this->groupBox5->Controls->Add(this->label2);
			this->groupBox5->Controls->Add(this->label6);
			this->groupBox5->Controls->Add(this->label3);
			this->groupBox5->Controls->Add(this->label7);
			this->groupBox5->Controls->Add(this->label4);
			this->groupBox5->Location = System::Drawing::Point(6, 6);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(387, 155);
			this->groupBox5->TabIndex = 18;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"Processor";
			// 
			// uCodeVersion
			// 
			this->uCodeVersion->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->uCodeVersion->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->uCodeVersion->Location = System::Drawing::Point(284, 115);
			this->uCodeVersion->Name = L"uCodeVersion";
			this->uCodeVersion->Size = System::Drawing::Size(84, 20);
			this->uCodeVersion->TabIndex = 27;
			this->uCodeVersion->Text = L" ";
			this->uCodeVersion->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label28
			// 
			this->label28->AutoSize = true;
			this->label28->Location = System::Drawing::Point(242, 118);
			this->label28->Name = L"label28";
			this->label28->Size = System::Drawing::Size(38, 13);
			this->label28->TabIndex = 26;
			this->label28->Text = L"uCode";
			// 
			// SupportedInstructionsLabel
			// 
			this->SupportedInstructionsLabel->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->SupportedInstructionsLabel->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->SupportedInstructionsLabel->Location = System::Drawing::Point(76, 118);
			this->SupportedInstructionsLabel->Name = L"SupportedInstructionsLabel";
			this->SupportedInstructionsLabel->Size = System::Drawing::Size(149, 34);
			this->SupportedInstructionsLabel->TabIndex = 25;
			this->SupportedInstructionsLabel->Text = L" ";
			this->SupportedInstructionsLabel->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			this->SupportedInstructionsLabel->Click += gcnew System::EventHandler(this, &MainView::SupportedInstructionsLabel_Click);
			// 
			// CPU_Revision
			// 
			this->CPU_Revision->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Revision->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Revision->Location = System::Drawing::Point(284, 92);
			this->CPU_Revision->Name = L"CPU_Revision";
			this->CPU_Revision->Size = System::Drawing::Size(45, 20);
			this->CPU_Revision->TabIndex = 24;
			this->CPU_Revision->Text = L" ";
			this->CPU_Revision->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_Stepping
			// 
			this->CPU_Stepping->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Stepping->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Stepping->Location = System::Drawing::Point(284, 71);
			this->CPU_Stepping->Name = L"CPU_Stepping";
			this->CPU_Stepping->Size = System::Drawing::Size(45, 20);
			this->CPU_Stepping->TabIndex = 23;
			this->CPU_Stepping->Text = L" ";
			this->CPU_Stepping->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_ExtendedModel
			// 
			this->CPU_ExtendedModel->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_ExtendedModel->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_ExtendedModel->Location = System::Drawing::Point(180, 92);
			this->CPU_ExtendedModel->Name = L"CPU_ExtendedModel";
			this->CPU_ExtendedModel->Size = System::Drawing::Size(45, 20);
			this->CPU_ExtendedModel->TabIndex = 22;
			this->CPU_ExtendedModel->Text = L" ";
			this->CPU_ExtendedModel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_Model
			// 
			this->CPU_Model->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Model->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Model->Location = System::Drawing::Point(180, 71);
			this->CPU_Model->Name = L"CPU_Model";
			this->CPU_Model->Size = System::Drawing::Size(45, 20);
			this->CPU_Model->TabIndex = 21;
			this->CPU_Model->Text = L" ";
			this->CPU_Model->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_ExtendedFamily
			// 
			this->CPU_ExtendedFamily->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_ExtendedFamily->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_ExtendedFamily->Location = System::Drawing::Point(76, 92);
			this->CPU_ExtendedFamily->Name = L"CPU_ExtendedFamily";
			this->CPU_ExtendedFamily->Size = System::Drawing::Size(45, 20);
			this->CPU_ExtendedFamily->TabIndex = 20;
			this->CPU_ExtendedFamily->Text = L" ";
			this->CPU_ExtendedFamily->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_Family
			// 
			this->CPU_Family->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Family->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Family->Location = System::Drawing::Point(76, 71);
			this->CPU_Family->Name = L"CPU_Family";
			this->CPU_Family->Size = System::Drawing::Size(45, 20);
			this->CPU_Family->TabIndex = 19;
			this->CPU_Family->Text = L" ";
			this->CPU_Family->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_Brand
			// 
			this->CPU_Brand->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Brand->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Brand->Location = System::Drawing::Point(76, 47);
			this->CPU_Brand->Name = L"CPU_Brand";
			this->CPU_Brand->Size = System::Drawing::Size(253, 20);
			this->CPU_Brand->TabIndex = 18;
			this->CPU_Brand->Text = L" ";
			this->CPU_Brand->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// CPU_Vendor
			// 
			this->CPU_Vendor->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->CPU_Vendor->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->CPU_Vendor->Location = System::Drawing::Point(76, 22);
			this->CPU_Vendor->Name = L"CPU_Vendor";
			this->CPU_Vendor->Size = System::Drawing::Size(253, 20);
			this->CPU_Vendor->TabIndex = 17;
			this->CPU_Vendor->Text = L" ";
			this->CPU_Vendor->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(7, 22);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(41, 13);
			this->label9->TabIndex = 16;
			this->label9->Text = L"Vendor";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(7, 125);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(61, 13);
			this->label8->TabIndex = 14;
			this->label8->Text = L"Instructions";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(7, 48);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(35, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Brand";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(234, 93);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(48, 13);
			this->label5->TabIndex = 12;
			this->label5->Text = L"Revision";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(7, 74);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(36, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Family";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(127, 93);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(54, 13);
			this->label6->TabIndex = 10;
			this->label6->Text = L"Ext.Model";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(127, 74);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(36, 13);
			this->label3->TabIndex = 4;
			this->label3->Text = L"Model";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(7, 93);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(51, 13);
			this->label7->TabIndex = 8;
			this->label7->Text = L"Ex.Family";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(234, 74);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(49, 13);
			this->label4->TabIndex = 6;
			this->label4->Text = L"Stepping";
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->button1);
			this->tabPage2->Controls->Add(this->groupBox4);
			this->tabPage2->Controls->Add(this->groupBox3);
			this->tabPage2->Controls->Add(this->groupBox2);
			this->tabPage2->Controls->Add(this->groupBox1);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(425, 342);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Caches";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(295, 302);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(25, 23);
			this->button1->TabIndex = 4;
			this->button1->Text = L"...";
			this->button1->TextAlign = System::Drawing::ContentAlignment::TopLeft;
			this->button1->UseVisualStyleBackColor = true;
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->label40);
			this->groupBox4->Controls->Add(this->Caches_L3_Size);
			this->groupBox4->Controls->Add(this->label38);
			this->groupBox4->Controls->Add(this->label16);
			this->groupBox4->Controls->Add(this->label17);
			this->groupBox4->Location = System::Drawing::Point(7, 228);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(323, 74);
			this->groupBox4->TabIndex = 3;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"L3 Cache";
			// 
			// label40
			// 
			this->label40->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label40->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->label40->Location = System::Drawing::Point(193, 21);
			this->label40->Name = L"label40";
			this->label40->Size = System::Drawing::Size(120, 20);
			this->label40->TabIndex = 30;
			this->label40->Text = L" ";
			this->label40->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// Caches_L3_Size
			// 
			this->Caches_L3_Size->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Caches_L3_Size->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->Caches_L3_Size->Location = System::Drawing::Point(67, 21);
			this->Caches_L3_Size->Name = L"Caches_L3_Size";
			this->Caches_L3_Size->Size = System::Drawing::Size(120, 20);
			this->Caches_L3_Size->TabIndex = 29;
			this->Caches_L3_Size->Text = L" ";
			this->Caches_L3_Size->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label38
			// 
			this->label38->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label38->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->label38->Location = System::Drawing::Point(67, 47);
			this->label38->Name = L"label38";
			this->label38->Size = System::Drawing::Size(246, 20);
			this->label38->TabIndex = 28;
			this->label38->Text = L" ";
			this->label38->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(6, 25);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(27, 13);
			this->label16->TabIndex = 21;
			this->label16->Text = L"Size";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(6, 51);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(55, 13);
			this->label17->TabIndex = 19;
			this->label17->Text = L"Descriptor";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->Caches_L2_Cores);
			this->groupBox3->Controls->Add(this->Caches_L2_Size);
			this->groupBox3->Controls->Add(this->label34);
			this->groupBox3->Controls->Add(this->label14);
			this->groupBox3->Controls->Add(this->label15);
			this->groupBox3->Location = System::Drawing::Point(7, 154);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(323, 74);
			this->groupBox3->TabIndex = 2;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"L2 Cache";
			// 
			// Caches_L2_Cores
			// 
			this->Caches_L2_Cores->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Caches_L2_Cores->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->Caches_L2_Cores->Location = System::Drawing::Point(193, 21);
			this->Caches_L2_Cores->Name = L"Caches_L2_Cores";
			this->Caches_L2_Cores->Size = System::Drawing::Size(120, 20);
			this->Caches_L2_Cores->TabIndex = 29;
			this->Caches_L2_Cores->Text = L" ";
			this->Caches_L2_Cores->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// Caches_L2_Size
			// 
			this->Caches_L2_Size->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Caches_L2_Size->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->Caches_L2_Size->Location = System::Drawing::Point(67, 21);
			this->Caches_L2_Size->Name = L"Caches_L2_Size";
			this->Caches_L2_Size->Size = System::Drawing::Size(120, 20);
			this->Caches_L2_Size->TabIndex = 28;
			this->Caches_L2_Size->Text = L" ";
			this->Caches_L2_Size->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label34
			// 
			this->label34->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label34->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->label34->Location = System::Drawing::Point(67, 47);
			this->label34->Name = L"label34";
			this->label34->Size = System::Drawing::Size(246, 20);
			this->label34->TabIndex = 27;
			this->label34->Text = L" ";
			this->label34->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(6, 25);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(27, 13);
			this->label14->TabIndex = 21;
			this->label14->Text = L"Size";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(6, 51);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(55, 13);
			this->label15->TabIndex = 19;
			this->label15->Text = L"Descriptor";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->label33);
			this->groupBox2->Controls->Add(this->Caches_L1D_Cores);
			this->groupBox2->Controls->Add(this->Caches_L1D_Size);
			this->groupBox2->Controls->Add(this->label12);
			this->groupBox2->Controls->Add(this->label13);
			this->groupBox2->Location = System::Drawing::Point(7, 80);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(323, 74);
			this->groupBox2->TabIndex = 1;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"L1D Cache";
			// 
			// label33
			// 
			this->label33->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label33->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->label33->Location = System::Drawing::Point(67, 47);
			this->label33->Name = L"label33";
			this->label33->Size = System::Drawing::Size(246, 20);
			this->label33->TabIndex = 26;
			this->label33->Text = L" ";
			this->label33->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// Caches_L1D_Cores
			// 
			this->Caches_L1D_Cores->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Caches_L1D_Cores->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->Caches_L1D_Cores->Location = System::Drawing::Point(193, 21);
			this->Caches_L1D_Cores->Name = L"Caches_L1D_Cores";
			this->Caches_L1D_Cores->Size = System::Drawing::Size(120, 20);
			this->Caches_L1D_Cores->TabIndex = 25;
			this->Caches_L1D_Cores->Text = L" ";
			this->Caches_L1D_Cores->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// Caches_L1D_Size
			// 
			this->Caches_L1D_Size->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Caches_L1D_Size->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->Caches_L1D_Size->Location = System::Drawing::Point(67, 22);
			this->Caches_L1D_Size->Name = L"Caches_L1D_Size";
			this->Caches_L1D_Size->Size = System::Drawing::Size(120, 20);
			this->Caches_L1D_Size->TabIndex = 24;
			this->Caches_L1D_Size->Text = L" ";
			this->Caches_L1D_Size->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(6, 25);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(27, 13);
			this->label12->TabIndex = 21;
			this->label12->Text = L"Size";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(6, 51);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(55, 13);
			this->label13->TabIndex = 19;
			this->label13->Text = L"Descriptor";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label30);
			this->groupBox1->Controls->Add(this->Caches_L1i_Cores);
			this->groupBox1->Controls->Add(this->Caches_L1i_Size);
			this->groupBox1->Controls->Add(this->label11);
			this->groupBox1->Controls->Add(this->label10);
			this->groupBox1->Location = System::Drawing::Point(7, 6);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(323, 74);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"L1I Cache";
			// 
			// label30
			// 
			this->label30->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label30->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->label30->Location = System::Drawing::Point(67, 47);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(246, 20);
			this->label30->TabIndex = 25;
			this->label30->Text = L" ";
			this->label30->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// Caches_L1i_Cores
			// 
			this->Caches_L1i_Cores->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Caches_L1i_Cores->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->Caches_L1i_Cores->Location = System::Drawing::Point(193, 22);
			this->Caches_L1i_Cores->Name = L"Caches_L1i_Cores";
			this->Caches_L1i_Cores->Size = System::Drawing::Size(120, 20);
			this->Caches_L1i_Cores->TabIndex = 24;
			this->Caches_L1i_Cores->Text = L" ";
			this->Caches_L1i_Cores->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// Caches_L1i_Size
			// 
			this->Caches_L1i_Size->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Caches_L1i_Size->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->Caches_L1i_Size->Location = System::Drawing::Point(67, 22);
			this->Caches_L1i_Size->Name = L"Caches_L1i_Size";
			this->Caches_L1i_Size->Size = System::Drawing::Size(120, 20);
			this->Caches_L1i_Size->TabIndex = 23;
			this->Caches_L1i_Size->Text = L" ";
			this->Caches_L1i_Size->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(6, 25);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(27, 13);
			this->label11->TabIndex = 21;
			this->label11->Text = L"Size";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(6, 51);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(55, 13);
			this->label10->TabIndex = 19;
			this->label10->Text = L"Descriptor";
			// 
			// tabPage3
			// 
			this->tabPage3->Controls->Add(this->UEFI_BIOS);
			this->tabPage3->Controls->Add(this->groupBox8);
			this->tabPage3->Location = System::Drawing::Point(4, 22);
			this->tabPage3->Name = L"tabPage3";
			this->tabPage3->Size = System::Drawing::Size(425, 342);
			this->tabPage3->TabIndex = 2;
			this->tabPage3->Text = L"MainBoard";
			this->tabPage3->UseVisualStyleBackColor = true;
			// 
			// UEFI_BIOS
			// 
			this->UEFI_BIOS->Controls->Add(this->label44);
			this->UEFI_BIOS->Controls->Add(this->UEFI_BIOS_Date);
			this->UEFI_BIOS->Controls->Add(this->UEFI_BIOS_Version);
			this->UEFI_BIOS->Controls->Add(this->UEFI_BIOS_Manufacture);
			this->UEFI_BIOS->Controls->Add(this->label42);
			this->UEFI_BIOS->Controls->Add(this->label43);
			this->UEFI_BIOS->Location = System::Drawing::Point(8, 123);
			this->UEFI_BIOS->Name = L"UEFI_BIOS";
			this->UEFI_BIOS->Size = System::Drawing::Size(352, 101);
			this->UEFI_BIOS->TabIndex = 2;
			this->UEFI_BIOS->TabStop = false;
			this->UEFI_BIOS->Text = L"UEFI/BIOS";
			// 
			// label44
			// 
			this->label44->AutoSize = true;
			this->label44->Location = System::Drawing::Point(37, 71);
			this->label44->Name = L"label44";
			this->label44->Size = System::Drawing::Size(30, 13);
			this->label44->TabIndex = 27;
			this->label44->Text = L"Date";
			// 
			// UEFI_BIOS_Date
			// 
			this->UEFI_BIOS_Date->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->UEFI_BIOS_Date->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->UEFI_BIOS_Date->Location = System::Drawing::Point(73, 67);
			this->UEFI_BIOS_Date->Name = L"UEFI_BIOS_Date";
			this->UEFI_BIOS_Date->Size = System::Drawing::Size(264, 20);
			this->UEFI_BIOS_Date->TabIndex = 26;
			this->UEFI_BIOS_Date->Text = L" ";
			this->UEFI_BIOS_Date->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// UEFI_BIOS_Version
			// 
			this->UEFI_BIOS_Version->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->UEFI_BIOS_Version->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->UEFI_BIOS_Version->Location = System::Drawing::Point(73, 47);
			this->UEFI_BIOS_Version->Name = L"UEFI_BIOS_Version";
			this->UEFI_BIOS_Version->Size = System::Drawing::Size(264, 20);
			this->UEFI_BIOS_Version->TabIndex = 25;
			this->UEFI_BIOS_Version->Text = L" ";
			this->UEFI_BIOS_Version->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// UEFI_BIOS_Manufacture
			// 
			this->UEFI_BIOS_Manufacture->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->UEFI_BIOS_Manufacture->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->UEFI_BIOS_Manufacture->Location = System::Drawing::Point(73, 27);
			this->UEFI_BIOS_Manufacture->Name = L"UEFI_BIOS_Manufacture";
			this->UEFI_BIOS_Manufacture->Size = System::Drawing::Size(264, 20);
			this->UEFI_BIOS_Manufacture->TabIndex = 23;
			this->UEFI_BIOS_Manufacture->Text = L" ";
			this->UEFI_BIOS_Manufacture->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// label42
			// 
			this->label42->AutoSize = true;
			this->label42->Location = System::Drawing::Point(6, 31);
			this->label42->Name = L"label42";
			this->label42->Size = System::Drawing::Size(67, 13);
			this->label42->TabIndex = 21;
			this->label42->Text = L"Manufacture";
			// 
			// label43
			// 
			this->label43->AutoSize = true;
			this->label43->Location = System::Drawing::Point(31, 51);
			this->label43->Name = L"label43";
			this->label43->Size = System::Drawing::Size(42, 13);
			this->label43->TabIndex = 19;
			this->label43->Text = L"Version";
			// 
			// groupBox8
			// 
			this->groupBox8->Controls->Add(this->Motheboard_Serial);
			this->groupBox8->Controls->Add(this->label46);
			this->groupBox8->Controls->Add(this->Motheboard_Revision);
			this->groupBox8->Controls->Add(this->Motheboard_Model);
			this->groupBox8->Controls->Add(this->Motheboard_Manufacture);
			this->groupBox8->Controls->Add(this->label32);
			this->groupBox8->Controls->Add(this->label35);
			this->groupBox8->Location = System::Drawing::Point(8, 12);
			this->groupBox8->Name = L"groupBox8";
			this->groupBox8->Size = System::Drawing::Size(357, 95);
			this->groupBox8->TabIndex = 1;
			this->groupBox8->TabStop = false;
			this->groupBox8->Text = L"Motheboard";
			// 
			// Motheboard_Serial
			// 
			this->Motheboard_Serial->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Motheboard_Serial->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->Motheboard_Serial->Location = System::Drawing::Point(73, 67);
			this->Motheboard_Serial->Name = L"Motheboard_Serial";
			this->Motheboard_Serial->Size = System::Drawing::Size(264, 20);
			this->Motheboard_Serial->TabIndex = 28;
			this->Motheboard_Serial->Text = L" ";
			this->Motheboard_Serial->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// label46
			// 
			this->label46->AutoSize = true;
			this->label46->Location = System::Drawing::Point(37, 71);
			this->label46->Name = L"label46";
			this->label46->Size = System::Drawing::Size(27, 13);
			this->label46->TabIndex = 27;
			this->label46->Text = L"S/N";
			// 
			// Motheboard_Revision
			// 
			this->Motheboard_Revision->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Motheboard_Revision->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->Motheboard_Revision->Location = System::Drawing::Point(228, 47);
			this->Motheboard_Revision->Name = L"Motheboard_Revision";
			this->Motheboard_Revision->Size = System::Drawing::Size(109, 20);
			this->Motheboard_Revision->TabIndex = 26;
			this->Motheboard_Revision->Text = L" ";
			this->Motheboard_Revision->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// Motheboard_Model
			// 
			this->Motheboard_Model->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Motheboard_Model->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->Motheboard_Model->Location = System::Drawing::Point(73, 47);
			this->Motheboard_Model->Name = L"Motheboard_Model";
			this->Motheboard_Model->Size = System::Drawing::Size(159, 20);
			this->Motheboard_Model->TabIndex = 25;
			this->Motheboard_Model->Text = L" ";
			this->Motheboard_Model->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// Motheboard_Manufacture
			// 
			this->Motheboard_Manufacture->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->Motheboard_Manufacture->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->Motheboard_Manufacture->Location = System::Drawing::Point(73, 27);
			this->Motheboard_Manufacture->Name = L"Motheboard_Manufacture";
			this->Motheboard_Manufacture->Size = System::Drawing::Size(264, 20);
			this->Motheboard_Manufacture->TabIndex = 23;
			this->Motheboard_Manufacture->Text = L" ";
			this->Motheboard_Manufacture->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// label32
			// 
			this->label32->AutoSize = true;
			this->label32->Location = System::Drawing::Point(6, 31);
			this->label32->Name = L"label32";
			this->label32->Size = System::Drawing::Size(67, 13);
			this->label32->TabIndex = 21;
			this->label32->Text = L"Manufacture";
			// 
			// label35
			// 
			this->label35->AutoSize = true;
			this->label35->Location = System::Drawing::Point(31, 51);
			this->label35->Name = L"label35";
			this->label35->Size = System::Drawing::Size(36, 13);
			this->label35->TabIndex = 19;
			this->label35->Text = L"Model";
			// 
			// tabPage5
			// 
			this->tabPage5->Location = System::Drawing::Point(4, 22);
			this->tabPage5->Name = L"tabPage5";
			this->tabPage5->Size = System::Drawing::Size(425, 342);
			this->tabPage5->TabIndex = 4;
			this->tabPage5->Text = L"Memory";
			this->tabPage5->UseVisualStyleBackColor = true;
			// 
			// tabPage4
			// 
			this->tabPage4->Location = System::Drawing::Point(4, 22);
			this->tabPage4->Name = L"tabPage4";
			this->tabPage4->Size = System::Drawing::Size(425, 342);
			this->tabPage4->TabIndex = 3;
			this->tabPage4->Text = L"Benchmarks";
			this->tabPage4->UseVisualStyleBackColor = true;
			// 
			// tabPage6
			// 
			this->tabPage6->Controls->Add(this->groupBox10);
			this->tabPage6->Controls->Add(this->groupBox9);
			this->tabPage6->Location = System::Drawing::Point(4, 22);
			this->tabPage6->Name = L"tabPage6";
			this->tabPage6->Size = System::Drawing::Size(425, 342);
			this->tabPage6->TabIndex = 5;
			this->tabPage6->Text = L"Meltdown/Spectre Protection status";
			this->tabPage6->UseVisualStyleBackColor = true;
			// 
			// groupBox10
			// 
			this->groupBox10->Controls->Add(this->KVAShadowPcidEnabled);
			this->groupBox10->Controls->Add(this->KVAShadowWindowsSupportEnabled);
			this->groupBox10->Controls->Add(this->KVAShadowWindowsSupportPresent);
			this->groupBox10->Controls->Add(this->KVAShadowRequired);
			this->groupBox10->Location = System::Drawing::Point(14, 173);
			this->groupBox10->Name = L"groupBox10";
			this->groupBox10->Size = System::Drawing::Size(362, 152);
			this->groupBox10->TabIndex = 3;
			this->groupBox10->TabStop = false;
			this->groupBox10->Text = L"Speculation control settings for CVE-2017-5754 [rogue data cache load]";
			// 
			// KVAShadowPcidEnabled
			// 
			this->KVAShadowPcidEnabled->AutoSize = true;
			this->KVAShadowPcidEnabled->Location = System::Drawing::Point(5, 98);
			this->KVAShadowPcidEnabled->Name = L"KVAShadowPcidEnabled";
			this->KVAShadowPcidEnabled->Size = System::Drawing::Size(278, 17);
			this->KVAShadowPcidEnabled->TabIndex = 4;
			this->KVAShadowPcidEnabled->Text = L"Windows OS support for PCID optimization is enabled";
			this->KVAShadowPcidEnabled->UseVisualStyleBackColor = true;
			// 
			// KVAShadowWindowsSupportEnabled
			// 
			this->KVAShadowWindowsSupportEnabled->AutoSize = true;
			this->KVAShadowWindowsSupportEnabled->Location = System::Drawing::Point(5, 75);
			this->KVAShadowWindowsSupportEnabled->Name = L"KVAShadowWindowsSupportEnabled";
			this->KVAShadowWindowsSupportEnabled->Size = System::Drawing::Size(281, 17);
			this->KVAShadowWindowsSupportEnabled->TabIndex = 3;
			this->KVAShadowWindowsSupportEnabled->Text = L"Windows OS support for kernel VA shadow is enabled";
			this->KVAShadowWindowsSupportEnabled->UseVisualStyleBackColor = true;
			// 
			// KVAShadowWindowsSupportPresent
			// 
			this->KVAShadowWindowsSupportPresent->AutoSize = true;
			this->KVAShadowWindowsSupportPresent->Location = System::Drawing::Point(5, 52);
			this->KVAShadowWindowsSupportPresent->Name = L"KVAShadowWindowsSupportPresent";
			this->KVAShadowWindowsSupportPresent->Size = System::Drawing::Size(278, 17);
			this->KVAShadowWindowsSupportPresent->TabIndex = 2;
			this->KVAShadowWindowsSupportPresent->Text = L"Windows OS support for kernel VA shadow is present";
			this->KVAShadowWindowsSupportPresent->UseVisualStyleBackColor = true;
			// 
			// KVAShadowRequired
			// 
			this->KVAShadowRequired->AutoSize = true;
			this->KVAShadowRequired->Location = System::Drawing::Point(6, 29);
			this->KVAShadowRequired->Name = L"KVAShadowRequired";
			this->KVAShadowRequired->Size = System::Drawing::Size(215, 17);
			this->KVAShadowRequired->TabIndex = 1;
			this->KVAShadowRequired->Text = L"Hardware requires kernel VA shadowing";
			this->KVAShadowRequired->UseVisualStyleBackColor = true;
			// 
			// groupBox9
			// 
			this->groupBox9->Controls->Add(this->BTIDisabledByNoHardwareSupport);
			this->groupBox9->Controls->Add(this->BTIDisabledBySystemPolicy);
			this->groupBox9->Controls->Add(this->BTIWindowsSupportEnabled);
			this->groupBox9->Controls->Add(this->BTIWindowsSupportPresent);
			this->groupBox9->Controls->Add(this->BTIHardwarePresent);
			this->groupBox9->Location = System::Drawing::Point(14, 13);
			this->groupBox9->Name = L"groupBox9";
			this->groupBox9->Size = System::Drawing::Size(376, 154);
			this->groupBox9->TabIndex = 2;
			this->groupBox9->TabStop = false;
			this->groupBox9->Text = L"Speculation control settings for CVE-2017-5715 [branch target injection]";
			// 
			// BTIDisabledByNoHardwareSupport
			// 
			this->BTIDisabledByNoHardwareSupport->AutoSize = true;
			this->BTIDisabledByNoHardwareSupport->Location = System::Drawing::Point(5, 122);
			this->BTIDisabledByNoHardwareSupport->Name = L"BTIDisabledByNoHardwareSupport";
			this->BTIDisabledByNoHardwareSupport->Size = System::Drawing::Size(506, 17);
			this->BTIDisabledByNoHardwareSupport->TabIndex = 6;
			this->BTIDisabledByNoHardwareSupport->Text = L"Windows OS support for branch target injection mitigation is disabled by absence "
				L"of hardware support:";
			this->BTIDisabledByNoHardwareSupport->UseVisualStyleBackColor = true;
			// 
			// BTIDisabledBySystemPolicy
			// 
			this->BTIDisabledBySystemPolicy->AutoSize = true;
			this->BTIDisabledBySystemPolicy->Location = System::Drawing::Point(5, 99);
			this->BTIDisabledBySystemPolicy->Name = L"BTIDisabledBySystemPolicy";
			this->BTIDisabledBySystemPolicy->Size = System::Drawing::Size(430, 17);
			this->BTIDisabledBySystemPolicy->TabIndex = 5;
			this->BTIDisabledBySystemPolicy->Text = L"Windows OS support for branch target injection mitigation is disabled by system p"
				L"olicy:";
			this->BTIDisabledBySystemPolicy->UseVisualStyleBackColor = true;
			// 
			// BTIWindowsSupportEnabled
			// 
			this->BTIWindowsSupportEnabled->AutoSize = true;
			this->BTIWindowsSupportEnabled->Location = System::Drawing::Point(5, 76);
			this->BTIWindowsSupportEnabled->Name = L"BTIWindowsSupportEnabled";
			this->BTIWindowsSupportEnabled->Size = System::Drawing::Size(347, 17);
			this->BTIWindowsSupportEnabled->TabIndex = 4;
			this->BTIWindowsSupportEnabled->Text = L"Windows OS support for branch target injection mitigation is enabled";
			this->BTIWindowsSupportEnabled->UseVisualStyleBackColor = true;
			// 
			// BTIWindowsSupportPresent
			// 
			this->BTIWindowsSupportPresent->AutoSize = true;
			this->BTIWindowsSupportPresent->Location = System::Drawing::Point(5, 52);
			this->BTIWindowsSupportPresent->Name = L"BTIWindowsSupportPresent";
			this->BTIWindowsSupportPresent->Size = System::Drawing::Size(344, 17);
			this->BTIWindowsSupportPresent->TabIndex = 3;
			this->BTIWindowsSupportPresent->Text = L"Windows OS support for branch target injection mitigation is present";
			this->BTIWindowsSupportPresent->UseVisualStyleBackColor = true;
			// 
			// BTIHardwarePresent
			// 
			this->BTIHardwarePresent->AutoSize = true;
			this->BTIHardwarePresent->Location = System::Drawing::Point(6, 29);
			this->BTIHardwarePresent->Name = L"BTIHardwarePresent";
			this->BTIHardwarePresent->Size = System::Drawing::Size(328, 17);
			this->BTIHardwarePresent->TabIndex = 2;
			this->BTIHardwarePresent->Text = L"Hardware support for branch target injection mitigation is present";
			this->BTIHardwarePresent->UseVisualStyleBackColor = true;
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->toolStripMenuItem1,
					this->overclockTunnerToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(433, 24);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// toolStripMenuItem1
			// 
			this->toolStripMenuItem1->Name = L"toolStripMenuItem1";
			this->toolStripMenuItem1->Size = System::Drawing::Size(52, 20);
			this->toolStripMenuItem1->Text = L"About";
			// 
			// overclockTunnerToolStripMenuItem
			// 
			this->overclockTunnerToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->intelSAIOVoltageCalculatorToolStripMenuItem,
					this->cPUPowerConsumationCalculatorToolStripMenuItem
			});
			this->overclockTunnerToolStripMenuItem->Name = L"overclockTunnerToolStripMenuItem";
			this->overclockTunnerToolStripMenuItem->Size = System::Drawing::Size(110, 20);
			this->overclockTunnerToolStripMenuItem->Text = L"Overclock utilites";
			// 
			// intelSAIOVoltageCalculatorToolStripMenuItem
			// 
			this->intelSAIOVoltageCalculatorToolStripMenuItem->Name = L"intelSAIOVoltageCalculatorToolStripMenuItem";
			this->intelSAIOVoltageCalculatorToolStripMenuItem->Size = System::Drawing::Size(362, 22);
			this->intelSAIOVoltageCalculatorToolStripMenuItem->Text = L"Intel SA/IO voltage calculator";
			// 
			// cPUPowerConsumationCalculatorToolStripMenuItem
			// 
			this->cPUPowerConsumationCalculatorToolStripMenuItem->Name = L"cPUPowerConsumationCalculatorToolStripMenuItem";
			this->cPUPowerConsumationCalculatorToolStripMenuItem->Size = System::Drawing::Size(362, 22);
			this->cPUPowerConsumationCalculatorToolStripMenuItem->Text = L"CPU Power Consumation calculator and  Core voltage ";
			// 
			// PollingCPUFrequncy
			// 
			this->PollingCPUFrequncy->Interval = 1000;
			this->PollingCPUFrequncy->Tick += gcnew System::EventHandler(this, &MainView::PollingCPUFrequncy_Tick);
			// 
			// statusStrip1
			// 
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->CPUVoltage, this->TemperatureCPU,
					this->CPU_Power_Package
			});
			this->statusStrip1->Location = System::Drawing::Point(0, 378);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(433, 22);
			this->statusStrip1->TabIndex = 2;
			this->statusStrip1->Text = L"statusStrip1";
			// 
			// CPUVoltage
			// 
			this->CPUVoltage->Name = L"CPUVoltage";
			this->CPUVoltage->Size = System::Drawing::Size(118, 17);
			this->CPUVoltage->Text = L"toolStripStatusLabel1";
			// 
			// TemperatureCPU
			// 
			this->TemperatureCPU->Name = L"TemperatureCPU";
			this->TemperatureCPU->Size = System::Drawing::Size(118, 17);
			this->TemperatureCPU->Text = L"toolStripStatusLabel1";
			// 
			// CPU_Power_Package
			// 
			this->CPU_Power_Package->Name = L"CPU_Power_Package";
			this->CPU_Power_Package->Size = System::Drawing::Size(117, 17);
			this->CPU_Power_Package->Text = L"CPU_Power_Package";
			// 
			// MainView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->ClientSize = System::Drawing::Size(433, 400);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->menuStrip1);
			this->DoubleBuffered = true;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->ImeMode = System::Windows::Forms::ImeMode::Disable;
			this->MainMenuStrip = this->menuStrip1;
			this->MaximizeBox = false;
			this->Name = L"MainView";
			this->Text = L"CPU Caps viewer";
			this->tabControl1->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->tabPage1->PerformLayout();
			this->groupBox7->ResumeLayout(false);
			this->groupBox7->PerformLayout();
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->tabPage2->ResumeLayout(false);
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->tabPage3->ResumeLayout(false);
			this->UEFI_BIOS->ResumeLayout(false);
			this->UEFI_BIOS->PerformLayout();
			this->groupBox8->ResumeLayout(false);
			this->groupBox8->PerformLayout();
			this->tabPage6->ResumeLayout(false);
			this->groupBox10->ResumeLayout(false);
			this->groupBox10->PerformLayout();
			this->groupBox9->ResumeLayout(false);
			this->groupBox9->PerformLayout();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void PollingCPUFrequncy_Tick(System::Object^  sender, System::EventArgs^  e) {

		_WMI_CPU->GetPropertyAsync();
		CPU_Current_Clock->Text = String::Format("{0}Mhz", _WMI_CPU->CurrentClock);
		DWORD H, L;
		DWORD TemperatureTarget;
		DWORD TemperatureTargetOffset;
		DWORD TjActivationTemperture;
		float Temperture;
		float CoreVoltage;
		double PowerPackageCurrent;
		int FIVRVoltage;
		DWORD CurrentPower;
		static DWORD PPower;
		static double PowerPackagePrev;


		if (Rdmsr(0x611, &L, &H))
		{
			
			PowerPackageCurrent = ((float)(L) * Energy_Power_Units);

			double PowerConsumed = PowerPackageCurrent - PowerPackagePrev;

			CPU_Power_Package->Text = String::Format("{0:0.000}W", PowerConsumed);

			PowerPackagePrev = PowerPackageCurrent;
		}
		if (Rdmsr(0x1A2, &L, &H))
		{

			TemperatureTarget = (L >> 16) & 0x7F;
			TemperatureTargetOffset = (L >> 24) & 0x1F;
			TjActivationTemperture = TemperatureTarget + TemperatureTargetOffset;

		}


		if (RdmsrTx(0x198, &L, &H, 0x1))
		{
			DWORD Extracted = H >> 32;
			DWORD FID = (H & 0x7FF) * 100;
			CoreVoltage = (float)((H >> 32)) / 8192;
			//CoreVoltage+= (float)((H >> 32) & 0x7FF) * ((float)1 / pow(2.0, 13));
			CPUVoltage->Text = String::Format("{0:0.000}V - {1}", CoreVoltage, Extracted);

		}


			if (RdmsrTx(0x19C, &L, &H, 0x1))
			{

				DWORD ResoltionInDegree = (L >> 27) & 0x7;
				DWORD ReadOut = (L >> 16) & 0x7F;
				Temperture = TjActivationTemperture - (ReadOut * ResoltionInDegree);

				// CoreVoltage = (float)((H >> 32)) / 8192;
				 //CoreVoltage+= (float)((H >> 32) & 0x7FF) * ((float)1 / pow(2.0, 13));
				TemperatureCPU->Text = String::Format("{0:0.000} C", Temperture);

			}






		}

		

private: System::Void SupportedInstructionsLabel_Click(System::Object^  sender, System::EventArgs^  e) {
	static bool Cliked = false;
	if (!_SupportedInstructionsForm->Visible)
		{
			
		if (_SupportedInstructionsForm->IsDisposed)
		{
			_SupportedInstructionsForm = gcnew  SupportedInstructionsForm();
		}
			_SupportedInstructionsForm->Show();
			 Cliked = true;

		}
		else
		{
			_SupportedInstructionsForm->Close();
			 Cliked = false;
			
		}

		 }
};
}

